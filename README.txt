
*******************************************************************************************************
* Booleangen User IP Rlease information to xilinx
* 
* data : 09/30/2017
* contact : sitao.mei@gmail.com
*******************************************************************************************************

--------------------------------------------------------------------------------------------------------------------------
Overlay information :
--------------------------------------------------------------------------------------------------------------------------
1)  Overlay : 
     1) Generate new "booleangen" on "base overlay"of "PYNQ 2017.01" release version. 
     2) dis-connect original "iop3/GPIO" IP from switchbox for "boolean gen" user IP. 

2)  "Boolean gen" user IP : 
      a) 24 cfgluts and input regiters set derived from Parimal's proposal. 
      b) add output configuration sets. 
      c) add mux chains .
      d) concurrent recfg fsm controls.

3)  deliveries : 
     a)  /overlay/top.bit(/tcl)  : generated bitstream and tcl, should be uploaded to SD card of board.
     b) /overlay/myip_boolen_gen1_0_s00_AXI.v :  user design for reference.
     c) rsc_report_impl.xlsl  :  fpga imple resource report .

Python information :
--------------------------------------------------------------------------------------------------------------------------
baseline : "PYNQ 2017.01" release version.

1) iop_const.py :    
     add new constants for :  "boolean gen" address definition.

2) boolgen.py : 
     user python package for "booleangen"user ip. 

3) deliveries : 
     please upload above files under path :  */python/iop/ 


Notebook information 
--------------------------------------------------------------------------------------------------------------------------
/notebook/*  : all bringup testcases. 


Documents :
-------------------------------------------------------------------------------------------------------------------------
./*.pptrx  :  presentation materials.


===================================================================

Notes:
1) Detail hardware resource will be analyzed and described at separated document as meeting. 