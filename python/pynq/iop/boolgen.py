'''

'''
from pynq import MMIO
from pynq.iop import iop
from pynq.iop import request_iop
from pynq.iop import iop_const
from pynq.iop import DevMode


"""Class cfglut primitive
This is primitive for Boolen gen inisde IOP
"""
class Cfglut ():
    def __init__ (self,usedflg,idx,ipins,opin,truthtbl):
        self.usedflg=usedflg
        self.idx=idx
        self.ipins=ipins
        self.opin=opin
        self.truthtbl=truthtbl
        
    @property
    def status (self):
        print ('cfglut used :',self.usedflg)
        print ('------ ipins mapping 4-0:',self.ipins)
        print ('------ opin mapping to :',self.opin)
        print ('------ truth tbl:',self.truthtbl)
     
    def daloc (self):
        self.usedflg=0
        
    def aloc (self):
        self.usedflg=1
         
        
"""Class mux4 primitive
This is primitive for Boolen gen inisde IOP
"""
class Mux4():
    def __init__(self,usedflg,iluts,opin):
        self.usedflg=usedflg
        self.opin=opin
        self.iluts=iluts
        
    @property
    def status(self):
        print('mux4 used: ',self.usedflg)       
        print('---- opin:',self.opin)
    def daloc (self):
        self.usedflg=0
        
    def aloc (self):
        self.usedflg=1

"""Class mux2 primitive
This is primitive for Boolen gen inisde IOP
"""
class Mux2():
    def __init__(self,usedflg,opin):
        self.usedflg=usedflg
        self.opin=opin
        
    @property
    def status(self):
        print('mux4 used: ',self.usedflg)
        print('---- opin:',self.opin)
        
    def daloc (self):
        self.usedflg=0
        
    def aloc (self):
        self.usedflg=1

"""Control IOP's Boolen Gen Resource allocation and de-allocation  . 
class Resource():
    
    This class will be responsible for Boolen Gen to allocate and de-allocate
    hw resource (including cfgluts,muxs).

    Attributes
    ----------
    ports : string
        Each arduino pin's attribute (idle,in,out)

    iop : devmode
        IO processor instance used by DevMode.

    cfgluts :list
        Boolen gen's cfgluts resource pool.
    
    muxs_0 :list
        Boolen gen's muxs resource pool for first level mux change.
    muxs_1 :list
        Boolen gen's muxs resource pool for second level mux change.
    muxs_2 :list
        Boolen gen's muxs resource pool for last level mux change.

    exprs_* : list
        function belonging rsource records.
"""
class Resource():
    def __init__(self):
        self.iop= DevMode(3, iop_const.ARDUINO_SWCFG_DIOALL) #Version : board 
#        self.iop=DevMode(3,iop_const) #version : sim
        '''
        0914 initial ardunio pin as inputs 0~19 
        '''
        self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_OUT_EN,0xfffff)
        #0914 ---end
        
        self.ports={}
        for idx in range(24):
            self.ports[idx] = 'IDLE'
        
        self.cfgluts=[]
        for idx in range(24):
            self.cfgluts.append(Cfglut(0,idx,[0,0,0,0,0],0,[0]))
            self.free_cnt_cfgluts=24
            
        self.muxs_0=[]            
        for idx in range(4):
            self.muxs_0.append(Mux4(0,[4*idx,4*idx+1,4*idx+2,4*idx+3],0))
            self.free_cnt_muxs_0=4
            
        self.muxs_1=[]            
        for idx in range(2):
            self.muxs_1.append(Mux2(0,0))  
            self.free_cnt_muxs_1=2
             
        self.muxs_2=[]            
        for idx in range(1):
            self.muxs_2.append(Mux2(0,0)) 
            self.free_cnt_cnt_muxs_2=1
        
        self.exprs={} 
        self.exprs_opin={}    
        self.exprs_cfgluts={}
        self.exprs_muxs0={}  
        self.exprs_muxs1={} 
        self.exprs_muxs2={}                                                             

    def rscDaloc(self,func_name):
        """
        Daloc : daloc cfgluts.
        """        
        #print ('Daloc exprs_cfgluts:',self.exprs_cfgluts[func_name],'Opin:',self.exprs_opin)
        if func_name in self.exprs_cfgluts :
            expr_cfgluts = self.exprs_cfgluts[func_name]   
        else :
            expr_cfgluts =[]
        if func_name in self.exprs_muxs0:
            expr_muxs0 =self.exprs_muxs0[func_name] 
        else:
            expr_muxs0 = []
        if func_name in self.exprs_muxs1:
            expr_muxs1 =self.exprs_muxs1[func_name] 
        else :
            expr_muxs1=[]
        if func_name in self.exprs_muxs2 :
            expr_muxs2 =self.exprs_muxs2[func_name]
        else:     
            expr_muxs2=[]
            

        """
        Daloc : clear opin to input.
        """
        if (func_name in self.exprs_opin):
            wdat = self.iop.read_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_OUT_EN) 
            for opin in self.exprs_opin[func_name]:
                wdat = wdat & ~(0x1<<opin) 
            self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_OUT_EN,wdat)
            #print ('Daloc opin :',self.exprs_opin,'for func:',self.exprs_cfgluts[func_name])

        for idx in expr_cfgluts : #(self.exprs_cfgluts[func_name])):
            self.cfgluts[idx].daloc() 
            self.free_cnt_cfgluts=self.free_cnt_cfgluts+1            

        for idx in expr_muxs0 : #(self.exprs_muxs0[func_name])):
            self.muxs_0[idx].daloc() 

        for idx in expr_muxs1 : #(self.exprs_muxs1[func_name])):
            self.muxs_1[idx].daloc()          

        for idx in expr_muxs2 : #(self.exprs_muxs2[func_name])):
            self.muxs_2[idx].daloc()       
        
        for idx in self.exprs_opin:
            self.ports[idx]='IDLE'    
       
        if ( func_name in self.exprs ):
            del self.exprs[func_name]                                                 
        if ( func_name in self.exprs_cfgluts):
            del self.exprs_cfgluts[func_name]
        if ( func_name in self.exprs_muxs0):
            del self.exprs_muxs0[func_name]  
        if ( func_name in self.exprs_muxs1):
            del self.exprs_muxs1[func_name]
        if (func_name in self.exprs_muxs2):          
            del self.exprs_muxs2[func_name]           
                              
        return 0
    
    def destroy(self):    
        """
        Clear dict
        """
        self.exprs.clear() 
        self.exprs_opin.clear()    
        self.exprs_cfgluts.clear()
        self.exprs_muxs0.clear()  
        self.exprs_muxs1.clear() 
        self.exprs_muxs2.clear()                                                            
        
        return 0  
                     
    def rscAloc(self,func_name,in_nums,ipins,opin,init_val,out_en,ipattern):
        """
        aloc : aloc boolen gen resource including :cfgluts,muxs,pins.
        """        
        if in_nums <= 5 : 
            if (self.free_cnt_cfgluts==0 or self.free_cnt_cfgluts>24 ):
                print ('rscAloc Fail: No available free cfgluts rsc! avaliable free cfgluts:',self.free_cnt_cfgluts,'function:',func_name,'.')
                return -1
            else:
                for cfglut in reversed(self.cfgluts) :
                    if ( cfglut.usedflg == 0 ):
                        # Aloc cfglut resource sucessfully 
                        print('Info : Allocaed cfglut',cfglut.idx,'for function:',func_name,'!')

                        # configure ipin & opin & truth table into allocated cfglut
                        self.set_ipin_cfgluts(cfglut.idx,ipins,ipattern)
                        #self.set_opin(cfglut.idx,opin,out_en) #0908 reverse
                        self.set_opin(opin,cfglut.idx,out_en)
                        self.set_truthtbl (cfglut.idx,init_val[0])
                        
                        # Recordes used logic resources
                        #self.exprs[func_name]=[in_nums,ipins,opin,init_val]
                        
                        # Aloc and recorde cfgluts resource
                        cfglut.aloc()
                        self.free_cnt_cfgluts=self.free_cnt_cfgluts-1
                        print('Free cfgluts after 5-inputs func:',self.free_cnt_cfgluts)
                        if (func_name not in self.exprs_cfgluts):
                            self.exprs_cfgluts[func_name]=[]
                        self.exprs_cfgluts[func_name].append(cfglut.idx)

                        # record opin allocation 
                        if ( out_en): 
                            if (func_name not in self.exprs_opin):
                                self.exprs_opin[func_name]=[]
                                self.exprs_opin[func_name].append(int(opin))
                                self.ports[opin] = func_name
                        #print('Boolgen- aloc exprs_cfgluts:',self.exprs_cfgluts,'cfglut idx',cfglut.idx,'ipin list:',ipins,'ipattern en:',ipattern,'opin:',opin,'out_en:',out_en)                           
                        return 0

                # Abnoraml exit with information print.

                print ('Fail : no avaliable cfglut resource for signle cfglut function with inputs ',in_nums,'function:',func_name,'!')
                print('-----------------------------------------------------------------------------------------')
                for idx in range(24):
                    print('       cfgluts',idx ,'used: ',self.cfgluts[idx].usedflg)
                print()
                return -1                    

        elif (in_nums==6 or in_nums==7): # 6/7 inputs user function - 4 cfgluts + 1 mux4

            # traverse cfgluts/muxs_0 resource from low to high order
            idx =0
            mux_grp=0
            num_cfgluts_grp=4
            while (idx<len(self.cfgluts)) :
                if ( (self.cfgluts[idx  ].usedflg==0) and (self.cfgluts[idx+1].usedflg==0) and 
                     (self.cfgluts[idx+2].usedflg==0) and (self.cfgluts[idx+3].usedflg==0) and 
                     (self.muxs_0 [int(idx/num_cfgluts_grp)].usedflg==0)  
                     ) :
                    # Record aloc function sucessfully
                    #self.exprs[func_name]=[in_nums,ipins,opin,init_val]

                    # loop to aloc and configure cfgluts
                    for ofst in range (num_cfgluts_grp) : 
                        self.set_ipin_cfgluts(idx+ofst,ipins[0: iop_const.CFGLUT_PINS_NUM +1],ipattern) # configure 0-4 inputs
                        self.set_truthtbl (idx+ofst,init_val[ofst])

                        # Aloc cfgluts resource and recorde
                        self.cfgluts[idx+ofst].aloc()
                        if (func_name not in self.exprs_cfgluts):
                            self.exprs_cfgluts[func_name]=[]
                        self.exprs_cfgluts[func_name].append(idx+ofst)
                    self.free_cnt_cfgluts=self.free_cnt_cfgluts-4

                    # configure mux's ipin and opin
                    grp0_idx=int(idx/num_cfgluts_grp)
                    grp0_ipins=[]
                    grp0_ipins.append(ipins[5])
                    print('ipins:',ipins,"in_nums:",in_nums)
                    if (len(ipins)==6):
                        grp0_ipins.append(ipins[5])
                    else:
                        grp0_ipins.append(ipins[6])
                    
                    self.set_ipin_mux(mux_grp,grp0_idx,grp0_ipins)#[iop_const.CFGLUT_PINS_NUM:iop_const.CFGLUT_PINS_NUM+2]) # configure 0-4 inputs

                    #set opin
                    self.set_opin(opin,int(iop_const.BGEN_MUX_BASE_G0+idx/num_cfgluts_grp),out_en) # configure mux grp's out pin

                    # Recordes used logic resources
                    self.muxs_0[int(idx/num_cfgluts_grp)].aloc()
                    if (func_name not in self.exprs_muxs0):
                        self.exprs_muxs0[func_name]=[]
                    self.exprs_muxs0[func_name].append(int(idx/num_cfgluts_grp))

                    if (func_name not in self.exprs_opin):
                        self.exprs_opin[func_name]=[]
                    self.exprs_opin[func_name].append(int(opin))
                    self.ports[opin] = func_name
                    
                    return 0 
                else :
                    idx=idx+num_cfgluts_grp

            # Abnoraml exit with information print.
            print ('Fail : no avaliable cfglut resource for multi cfglut function with inputs ',in_nums,',functions:',func_name,'!')
            for idx in range(24):
                print('       cfgluts',idx ,'used: ',self.cfgluts[idx].usedflg)
            for idx in range(4):
                print('       muxs_0',idx ,'used: ',self.muxs_0[idx].usedflg)

            return -1

        elif (in_nums==8): # 8 inputs functions - require : 8 cfgluts + 2 mux4 + 1 mux2
            idx =0
            mux_grp=1
            num_cfgluts_grp=8
            while (idx<=len(self.cfgluts)) :
                if ( (self.cfgluts[idx  ].usedflg==0) or (self.cfgluts[idx+1].usedflg==0) or 
                     (self.cfgluts[idx+2].usedflg==0) or (self.cfgluts[idx+3].usedflg==0) or 
                     (self.cfgluts[idx+4].usedflg==0) or (self.cfgluts[idx+5].usedflg==0) or 
                     (self.cfgluts[idx+6].usedflg==0) or (self.cfgluts[idx+7].usedflg==0) or
                     (self.muxs_0 [idx/num_cfgluts_grp].usedflg==0) or (self.muxs_0[idx/num_cfgluts_grp+1].usedflg==0) or
                     (self.muxs_1 [idx/num_cfgluts_grp].usedflg==0) 
                     ) :
                    # Record aloc function sucessfully
                    #self.exprs[func_name]=[in_nums,ipins,opin,init_val]

                    # aloc and configure cfgluts
                    for ofst in range (num_cfgluts_grp) : 
                        self.set_ipin_cfgluts(idx+ofst,ipins[0:iop_const.CFGLUT_PINS_NUM+1],ipattern) # configure 0-4 inputs
                        self.set_truthtbl (idx+ofst,init_val[ofst])

                        self.cfgluts[idx+ofst].aloc()
                        if (func_name not in self.exprs_cfgluts):
                            self.exprs_cfgluts[func_name]=[]
                        self.exprs_cfgluts[func_name].append(idx+ofst)
                    self.free_cnt_cfgluts=self.free_cnt_cfgluts-8

                    # configure mux's ipin and opin
                    #Group 1
                    grp1_idx=int(idx/num_cfgluts_grp)
                    self.set_ipin_mux(1,grp1_idx,ipins[7])#[iop_const.CFGLUT_PINS_NUM+1:iop_const.CFGLUT_PINS_NUM+3]) # configure 5-6th inputs
                    #Group 0
                    self.set_ipin_mux(0,int(2*grp1_idx),ipins[5:7])
                    self.set_ipin_mux(0,int(2*grp1_idx+1),ipins[5:7])                                      
                    self.set_opin(opin,int(iop_const.BGEN_MUX_BASE_G1+idx/num_cfgluts_grp),out_en) # configure mux grp's out pin

                    # Recordes used logic resources
                    self.muxs_0[int(idx/num_cfgluts_grp)].aloc()
                    self.muxs_0[int(idx/num_cfgluts_grp)+1].aloc()
                    if (func_name not in self.exprs_muxs0):
                        self.exprs_muxs0[func_name]=[]
                    self.exprs_muxs0[func_name].append(int(idx/num_cfgluts_grp))
                    self.exprs_muxs0[func_name].append(int(idx/num_cfgluts_grp+1))

                    self.muxs_1[int(idx/num_cfgluts_grp)].aloc()
                    if (func_name not in self.exprs_muxs1):
                        self.exprs_muxs1[func_name]=[]
                    self.exprs_muxs1[func_name].append(int(idx/num_cfgluts_grp))

                    if (func_name not in self.exprs_opin):
                        self.exprs_opin[func_name]=[]
                    self.exprs_opin[func_name].append(int(opin))
                    self.ports[opin] = func_name

                    return 0 
                else :
                    idx=idx+num_cfgluts_grp

            # Abnoraml exit with information print.
            print ('Fail : no avaliable boolengen resource for multi cfglut function with inputs ',in_nums,',functions:',func_name,'!')
            for idx in range(24):
                print('       cfgluts',idx ,'used: ',self.cfgluts[idx].usedflg)
            for idx in range(4):
                print('       muxs_0',idx ,'used: ',self.muxs_0[idx].usedflg)
            for idx in range(2):
                print('       muxs_1',idx ,'used: ',self.muxs_1[idx].usedflg)
            return -1

        elif (in_nums==9):
            idx =0
            mux_grp=2
            num_cfgluts_grp=16

            if ( (self.cfgluts[idx   ].usedflg==0) or (self.cfgluts[idx+ 1].usedflg==0) or 
                 (self.cfgluts[idx+ 2].usedflg==0) or (self.cfgluts[idx+ 3].usedflg==0) or 
                 (self.cfgluts[idx+ 4].usedflg==0) or (self.cfgluts[idx+ 5].usedflg==0) or 
                 (self.cfgluts[idx+ 6].usedflg==0) or (self.cfgluts[idx+ 7].usedflg==0) or 
                 (self.cfgluts[idx+ 8].usedflg==0) or (self.cfgluts[idx+ 9].usedflg==0) or 
                 (self.cfgluts[idx+10].usedflg==0) or (self.cfgluts[idx+11].usedflg==0) or 
                 (self.cfgluts[idx+12].usedflg==0) or (self.cfgluts[idx+13].usedflg==0) or 
                 (self.cfgluts[idx+14].usedflg==0) or (self.cfgluts[idx+15].usedflg==0)  
                 ) :
                # Record aloc function sucessfully
                #self.exprs[func_name]=[in_nums,ipins,opin,init_val]

                for ofst in range (num_cfgluts_grp) : 
                    self.set_ipin_cfgluts(idx+ofst,ipins[0:iop_const.CFGLUT_PINS_NUM+1],ipattern) # configure 0-4 inputs
                    self.set_truthtbl (idx+ofst,init_val[ofst])

                    self.cfgluts[idx+ofst].aloc()
                    if (func_name not in self.exprs_cfgluts):
                        self.exprs_cfgluts[func_name]=[]
                    self.exprs_cfgluts[func_name].append(idx+ofst)
                self.free_cnt_cfgluts=self.free_cnt_cfgluts-16
                print('Free cfgluts after 9-inputs func:',self.free_cnt_cfgluts)

                # configure mux's ipin and opin
                self.set_ipin_mux(0,0,ipins[5:7]) # configure 5-6th inputs
                self.set_ipin_mux(0,1,ipins[5:7]) # configure 5-6th inputs
                self.set_ipin_mux(0,2,ipins[5:7]) # configure 5-6th inputs
                self.set_ipin_mux(0,3,ipins[5:7]) # configure 5-6th inputs

                self.set_ipin_mux(1,0,ipins[7]) # configure 7th inputs
                self.set_ipin_mux(1,1,ipins[7]) # configure 7th inputs
                self.set_ipin_mux(2,0,ipins[8]) # configure 8th inputs

                self.set_opin(opin,int(iop_const.BGEN_MUX_BASE_G2+idx/num_cfgluts_grp),out_en) # configure mux grp's out pin

                # Recordes used logic resources
                for i in range (4) :
                    self.muxs_0[i].aloc()
                    if (func_name not in self.exprs_muxs0):
                        self.exprs_muxs0[func_name]=[]
                    self.exprs_muxs0[func_name].append(int(i))

                for i in range (2) :
                    self.muxs_1[i].aloc()
                    if (func_name not in self.exprs_muxs1):
                        self.exprs_muxs1[func_name]=[]
                    self.exprs_muxs1[func_name].append(int(i))

                self.muxs_2[0].aloc()
                if (func_name not in self.exprs_muxs2):
                    self.exprs_muxs2[func_name]=[]
                self.exprs_muxs2[func_name].append(int(0))

                if (func_name not in self.exprs_opin):
                    self.exprs_opin[func_name]=[]
                self.exprs_opin[func_name].append(int(opin))
                self.ports[opin] = func_name           

                return 0 

            else :
                # Abnoraml exit with information print.
                print ('Fail : no avaliable boolengen resource for multi cfglut function with inputs ',in_nums,',functions:',func_name,'!')
                for idx in range(24):
                    print('       cfgluts',idx ,'used: ',self.cfgluts[idx].usedflg)
                for idx in range(4):
                    print('       muxs_0',idx ,'used: ',self.muxs_0[idx].usedflg)
                for idx in range(2):
                    print('       muxs_1',idx ,'used: ',self.muxs_1[idx].usedflg)
                print('       muxs_2 used: ',self.muxs_2[0].usedflg)

                return -1

        else:
            print ('Fail: unsupported too many inputs for function:',func_name,'!')
            return -1
    
    def set_ipin_cfgluts (self,idx,ipins_lst,ipattern):    
        """
        set input pins for allocated cfglut resource 
        """        
        
        wdat = self.iop.read_cmd(iop_const.BGEN_BASE_ADDR+8*idx)
        
        """
        reg 2*n
        1) configure ipins.
        2) configure ipattern_en
        """
        mask = 0x1f
        pin_idx=0
        for ipin_id in ipins_lst:
            #print ('mask:',bin(mask << (idx*iop_const.CFGLUT_PINS_NUM)))
            wdat = wdat & ~(mask<<(pin_idx*iop_const.CFGLUT_PINS_NUM)) | (ipin_id<<(pin_idx*iop_const.CFGLUT_PINS_NUM))
            pin_idx+=1
            #print ('Set_ipin wdat:',hex(wdat),'mask: ', bin(mask),'ipins:',ipins_lst)

            '''configure ipin's input en -BEGIN
            0914 - fix tri_en reverse cfg issue.
            if ( ipin_id <20 ):
                wdat = self.iop.read_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_OUT_EN) 
                wdat = wdat | (0x1<<ipin_id)#opin)
                self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_OUT_EN,wdat)
            '''

        if (ipattern==1):
            wdat=wdat | 0x80000000
        
        self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+8*idx,wdat)                   
        #print ('Boolgen- set_ipin idx:',idx, 'ipins:',ipins_lst,'ipatters:',ipattern,'wdat:',hex(wdat))
      

    def set_ipin_mux (self,mux_grp,idx,ipins_lst):    
        """
        set input pins for muxs resource 
        {mux_grp,idx} :
        1) grp0 , idx0 :  mux4 -> cfglut0-3  
           BGEN_BASE_MUXS_IPINS + 4 / 8 
        """         
        mask = 0x1f
        addr= iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_MUXS_IPINS
        if (mux_grp==2) : # level 2 mux2 - 1 input 
            rdat = self.iop.read_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_MUXS_IPINS)
            wdat = rdat & ~(mask<<(iop_const.BGEN_POS_MUXS_IPINS_G2)) | (ipins_lst<<iop_const.BGEN_POS_MUXS_IPINS_G2)
            self.iop.write_cmd(addr,wdat)                   

        elif ( mux_grp==1 ) : # level 1 mux2 - 2*1 inputs
            rdat = self.iop.read_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_MUXS_IPINS)

            wdat = rdat & ~(mask<< ( idx*iop_const.CFGLUT_PINS_NUM) ) | (ipins_lst<<(idx*iop_const.CFGLUT_PINS_NUM) )
            self.iop.write_cmd(addr,wdat)                   

        elif ( mux_grp==0 ): # level 0 mux4 - 4*2 inputs
            addr = addr + 4
            rdat = self.iop.read_cmd(addr)
            ofst_pos=idx      
            wdat = rdat & ~(mask<< (ofst_pos*iop_const.CFGLUT_PINS_NUM) ) | (ipins_lst[0]<<(ofst_pos*iop_const.CFGLUT_PINS_NUM) )
            self.iop.write_cmd(addr,wdat)    

            addr = addr+4
            rdat = self.iop.read_cmd(addr)
            wdat = rdat & ~(mask<< (ofst_pos*iop_const.CFGLUT_PINS_NUM) ) | (ipins_lst[1]<<(ofst_pos*iop_const.CFGLUT_PINS_NUM) )
            self.iop.write_cmd(addr,wdat) 
        else :
            print ('Error, Abnormal inputs <mux_grp> when setting muxs inputs for functions:!') 

    def set_opin (self,idx,opin,out_en):
        """
        set ouput pins for allocated cfglut resource 
        """        
        
        #Configure output pin mapping
        rdat = self.iop.read_cmd(iop_const.BGEN_BASE_ADDR+8*idx)
        mask = 0x1f<< (5*iop_const.CFGLUT_PINS_NUM)
        #print('Boolgen- set_opin idx:', idx,'opin:',opin,'out_en:',out_en,'wdat:',rdat)
        wdat = rdat & ~mask | (opin<< (5*iop_const.CFGLUT_PINS_NUM)) 
        self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+8*idx,wdat)
        
        #Write output pin's enable when require.
        if (out_en==1):
            wdat = self.iop.read_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_OUT_EN) 
            #0914 - fix tri_en reverse cfg issue.
            if ( idx > 19 ) :
                wdat = wdat | (0x1<<idx)#opin)
            else :
                wdat = wdat | (0x0<<idx)#opin)
            self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_OUT_EN,wdat)

        #print('Boolgen- set_opin wdat_out_en:',hex(wdat))
        
    
    def set_truthtbl (self,idx,init_val):
        """
        set truth tbl for allocated cfglut resource 
        """        
        self.iop.write_cmd((iop_const.BGEN_BASE_ADDR+8*idx+4),init_val)
      
        '''
        write reconfg start
        '''
        cfg_start=0x1<<idx
        self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_RCFG_START,0) 
        self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_RCFG_START,cfg_start)#0xfffff) 
        self.iop.write_cmd(iop_const.BGEN_BASE_ADDR+iop_const.BGEN_OFST_RCFG_START,0)  

        print ('Boolgen - set_truthtbl idx:',idx,'init val:',hex(init_val))
        return 0
    
    def status_func(self,func_name):
        #print('-----------------------------------------------------------------------')
        if (func_name in self.exprs):
            print('Function name: ',func_name, '=',self.exprs[func_name])
        if (func_name in self.exprs_cfgluts) :   
            print('                Used cfgluts: ',self.exprs_cfgluts[func_name])
        if (func_name in self.exprs_muxs0) :
            print('                Used mux0   : ',self.exprs_muxs0[func_name])
        if (func_name in self.exprs_muxs1):
            print('                Used mux1   : ',self.exprs_muxs1[func_name])   
        if (func_name in self.exprs_muxs2):        
            print('                Used mux2   : ',self.exprs_muxs2[func_name])
        #print('-----------------------------------------------------------------------')
        
                    
    def status(self):
        print('=======================================================================')                
        print('                     Out Ports')
        #for k,v in self.ports.items() :
        #    print(k,'->',v)
        print(self.ports)
        print('                     Functions')
        print('-----------------------------------------------------------------------')  
        for func_name,bexpr in self.exprs.items() :      
            self.status_func(func_name)
                    
        #print('=======================================================================')       
        print()
        print('Avaliable free cfgluts:',self.free_cnt_cfgluts)
        print('Cfgluts : 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23')
        print('-----------------------------------------------------------------------')
        print('         ',
              self.cfgluts[ 0].usedflg,self.cfgluts[ 1].usedflg,
              self.cfgluts[ 2].usedflg,self.cfgluts[ 3].usedflg,
              self.cfgluts[ 4].usedflg,self.cfgluts[ 5].usedflg,
              self.cfgluts[ 6].usedflg,self.cfgluts[ 7].usedflg,
              self.cfgluts[ 8].usedflg,self.cfgluts[ 9].usedflg,
              '',self.cfgluts[10].usedflg,'',self.cfgluts[11].usedflg,
              '',self.cfgluts[12].usedflg,'',self.cfgluts[13].usedflg,
              '',self.cfgluts[14].usedflg,'',self.cfgluts[15].usedflg,
              '',self.cfgluts[16].usedflg,'',self.cfgluts[17].usedflg,
              '',self.cfgluts[18].usedflg,'',self.cfgluts[19].usedflg,
              '',self.cfgluts[20].usedflg,'',self.cfgluts[21].usedflg,
              '',self.cfgluts[22].usedflg,'',self.cfgluts[23].usedflg
              )        
        #print()
        #print('-----------------------------------------------------------------------')
        print('muxs level0 :',
              self.muxs_0[ 0].usedflg,self.muxs_0[ 1].usedflg,
              self.muxs_0[ 2].usedflg,self.muxs_0[ 3].usedflg 
            )
        #print('-----------------------------------------------------------------------')
        print('muxs level1 :',
              self.muxs_1[ 0].usedflg,self.muxs_1[ 1].usedflg
            )
        #print()
        print('muxs level2 :',
              self.muxs_2[ 0].usedflg
            )
 
      
class Boolgen():
    """Boolen Gen operations  . 
    
    This class will be responsible for Boolen Gen expresion decode and allocate from resource

    Attributes
    ----------
    """
    bgen_exprs={}
    port_inout={}

    bgen_func_cnt = 0
    
    def __init__(self):
    #    self.bgen_exprs[func_name]=[expr,in_nums]
        self.rsc=Resource()
    
       
    def set(self,func_name,ivars,ovar,bexpr,out_en=1,ipattern=0):
        """Boolen Gen set function  
        Parse input function, Then allocate resource from Resource() for in function. 

        Attributes
        ----------
        """
        '''0914 fix 6-inputs aligment with 7-input issue
        '''
        if (len(ivars)==6) :
            ivars.append(ivars[5])
        varnum=len(ivars)

        ivals_name=[]
        for bidx in ivars:
            tmp='p'
            tmp+=str(bidx)   
            ivals_name.append(tmp) 

        print('function:',func_name,'inputs:',ivals_name,'expression:',bexpr,'varnum',varnum)
        print('-----------------------------')
       
        truthtbl=[]
        for idx in range(2**varnum):
            #bvec = list(bin(idx)[2:].zfill(varnum))  
            bvec_pre = list(bin(idx)[2:].zfill(varnum))  
            bvec=list(reversed(bvec_pre)) #0908 reversed to match lists order.
            #print ('[boolgen] bvec:',bvec)             
            
            #print('idx:',idx,'binary:',bvec)
            
            pidx=0
            flg_rep_inv=0
            ivals_dat={}            
            for ipin in ivals_name:#ivars:
                #tmp='p'
                #tmp+=str(bidx)   
                #ivals_name.append(tmp) 
                
                tmp_dat= bvec[pidx]   
                #print ('idx:',idx,'ipin:',ipin,'ivals_dat:',ivals_dat,'tmp_dat:',tmp_dat,'rep_inv:',flg_rep_inv) 
                if ( ipin in ivals_dat) :
                    if (int(tmp_dat)==int(ivals_dat[ipin])):
                        ivals_dat[ipin]=tmp_dat      
                    else:
                        flg_rep_inv = 1 
                        break
                else:
                    ivals_dat[ipin]=tmp_dat       
                exec("%s=%d" % (ipin,int(bvec[pidx],2) ))  
                #print ('[boolgen] p',bidx,':',tmp)          ,2         
                pidx +=1
                #print ('---ipin:',ipin,'ivals_dat:',ivals_dat,'rep_inv:',flg_rep_inv)       
            if (flg_rep_inv==0):            
                x = eval(bexpr)
            else:
                x=0

            if ( (x%2) != 0 ):
                x=1
            else :
                x=0
            #print ('[boolgen] x:',x,'ivals_dat:',ivals_dat,'idx:',idx)   
            truthtbl.append(x)            
            #print(ivals_dat,'                :',x)
            #print(ivals_lst,'   :',x)
        
        init_val = [] 
        #for idx in range (len(truthtbl)):
        idx=0
        while idx < len(truthtbl) : 
            init_val.append(int(''.join(map(str,reversed(truthtbl[idx:idx+32]))),base=2))
            idx=idx+32
#        init_val=int(''.join(map(str,reversed(truthtbl))),base=2)
#        print(truthtbl,'init_val:',hex(init_val))
        flg=self.rsc.rscAloc(func_name, varnum, ivars, int(ovar) ,init_val,out_en,ipattern)
        if (flg==0):
            self.rsc.exprs[func_name]=bexpr

    def clr(self,func_name):
        """Boolen Gen clr function  
        De-allocate resource from Resource() for function to be destroied. 

        Attributes
        ----------
        """
        return self.rsc.rscDaloc (func_name)
          
    def destroy(self):
        func_list = list(self.rsc.exprs.keys())
        for func_name in func_list :    
            self.rsc.rscDaloc(func_name)
            
        return self.rsc.destroy()
            
    def status(self):
        self.rsc.status()


