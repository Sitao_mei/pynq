
`timescale 1 ns / 1 ps

	module myip_boolen_gen_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 8
	)
	(
		// Users to add ports here
		// ******* Shield side
	    	// analog channels
	   	input [5:0] shield2sw_data_in_a5_a0,
   		output [5:0] sw2shield_data_out_a5_a0,
   		output [5:0] sw2shield_tri_out_a5_a0,  
   		//    input [5:0] analog_p_in, 
   		//    input [5:0] analog_n_in, 
   		// digital channels
   		input [1:0] shield2sw_data_in_d1_d0,
   		output [1:0] sw2shield_data_out_d1_d0,
   		output [1:0] sw2shield_tri_out_d1_d0,
   		input [11:0] shield2sw_data_in_d13_d2,
   		output [11:0] sw2shield_data_out_d13_d2,
   		output [11:0] sw2shield_tri_out_d13_d2,	
		//****** PBS
		input wire [3:0]	pbs ,
		//****** LEDS
		output wire [3:0]	leds ,
//		input wire [23:0]	PINS_SHIELD_I ,
//		output wire [23:0]	PINS_SHIELD_OUT_EN ,
//		output wire [23:0]	PINS_SHIELD_O ,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
// Instantiation of Axi Bus Interface S00_AXI
	myip_boolen_gen_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) myip_boolen_gen_v1_0_S00_AXI_inst (
		//********User added ports****************
		.PINS_SHIELD_I 		({pbs[3:0],4'h0,shield2sw_data_in_a5_a0[5:0],shield2sw_data_in_d1_d0[1:0],shield2sw_data_in_d13_d2[11:0]} ),
		.PINS_SHIELD_OUT_EN 	({sw2shield_tri_out_a5_a0[5:0],sw2shield_tri_out_d1_d0[1:0],sw2shield_tri_out_d13_d2[11:0]} ),
		.PINS_SHIELD_O 		({leds[3:0],sw2shield_data_out_a5_a0[5:0],sw2shield_data_out_d1_d0[1:0],sw2shield_data_out_d13_d2[11:0]} 		),
		//********User added ports End****************
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
