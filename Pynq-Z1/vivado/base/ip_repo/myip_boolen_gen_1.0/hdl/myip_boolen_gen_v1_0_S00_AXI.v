
`timescale 1 ns / 1 ps

	module myip_boolen_gen_v1_0_S00_AXI #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 8
	)
	(
		// Users to add ports here
		input wire [27:0]	PINS_SHIELD_I , // {pbs,4'h0,pins[19:0]}
		output wire [23:0]	PINS_SHIELD_OUT_EN ,
		output reg [23:0]	PINS_SHIELD_O ,
		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY
	);

	// AXI4LITE signals
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
	reg  	axi_awready;
	reg  	axi_wready;
	reg [1 : 0] 	axi_bresp;
	reg  	axi_bvalid;
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
	reg  	axi_arready;
	reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;
	reg [1 : 0] 	axi_rresp;
	reg  	axi_rvalid;

	// Example-specific design signals
	// local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	// ADDR_LSB is used for addressing 32/64 bit registers/memories
	// ADDR_LSB = 2 for 32 bits (n downto 2)
	// ADDR_LSB = 3 for 64 bits (n downto 3)
	localparam integer ADDR_LSB = (C_S_AXI_DATA_WIDTH/32) + 1;
	localparam integer OPT_MEM_ADDR_BITS = 5;
	//----------------------------------------------
	//-- Signals for user logic register space example
	//------------------------------------------------
	//-- Number of Slave Registers 56
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg4;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg5;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg6;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg7;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg8;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg9;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg10;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg11;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg12;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg13;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg14;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg15;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg16;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg17;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg18;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg19;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg20;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg21;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg22;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg23;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg24;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg25;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg26;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg27;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg28;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg29;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg30;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg31;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg32;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg33;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg34;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg35;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg36;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg37;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg38;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg39;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg40;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg41;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg42;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg43;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg44;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg45;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg46;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg47;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg48;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg49;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg50;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg51;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg52;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg53;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg54;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg55;
	wire	 slv_reg_rden;
	wire	 slv_reg_wren;
	reg [C_S_AXI_DATA_WIDTH-1:0]	 reg_data_out;
	integer	 byte_index;

	// I/O Connections assignments

	assign S_AXI_AWREADY	= axi_awready;
	assign S_AXI_WREADY	= axi_wready;
	assign S_AXI_BRESP	= axi_bresp;
	assign S_AXI_BVALID	= axi_bvalid;
	assign S_AXI_ARREADY	= axi_arready;
	assign S_AXI_RDATA	= axi_rdata;
	assign S_AXI_RRESP	= axi_rresp;
	assign S_AXI_RVALID	= axi_rvalid;
	// Implement axi_awready generation
	// axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	// de-asserted when reset is low.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // slave is ready to accept write address when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_awready <= 1'b1;
	        end
	      else           
	        begin
	          axi_awready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_awaddr latching
	// This process is used to latch the address when both 
	// S_AXI_AWVALID and S_AXI_WVALID are valid. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awaddr <= 0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // Write Address latching 
	          axi_awaddr <= S_AXI_AWADDR;
	        end
	    end 
	end       

	// Implement axi_wready generation
	// axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	// de-asserted when reset is low. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_wready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID)
	        begin
	          // slave is ready to accept write data when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_wready <= 1'b1;
	        end
	      else
	        begin
	          axi_wready <= 1'b0;
	        end
	    end 
	end       

	// Implement memory mapped register select and write logic generation
	// The write data is accepted and written to memory mapped registers when
	// axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	// select byte enables of slave registers while writing.
	// These registers are cleared when reset (active low) is applied.
	// Slave register write enable is asserted when valid address and data are available
	// and the slave is ready to accept the write address and write data.
	assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      slv_reg0 <= 32'h18;//0; // user - default to led0
	      slv_reg1 <= 0;
	      slv_reg2 <= 0;
	      slv_reg3 <= 0;
	      slv_reg4 <= 0;
	      slv_reg5 <= 0;
	      slv_reg6 <= 0;
	      slv_reg7 <= 0;
	      slv_reg8 <= 0;
	      slv_reg9 <= 0;
	      slv_reg10 <= 0;
	      slv_reg11 <= 0;
	      slv_reg12 <= 0;
	      slv_reg13 <= 0;
	      slv_reg14 <= 0;
	      slv_reg15 <= 0;
	      slv_reg16 <= 0;
	      slv_reg17 <= 0;
	      slv_reg18 <= 0;
	      slv_reg19 <= 0;
	      slv_reg20 <= 0;
	      slv_reg21 <= 0;
	      slv_reg22 <= 0;
	      slv_reg23 <= 0;
	      slv_reg24 <= 0;
	      slv_reg25 <= 0;
	      slv_reg26 <= 0;
	      slv_reg27 <= 0;
	      slv_reg28 <= 0;
	      slv_reg29 <= 0;
	      slv_reg30 <= 0;
	      slv_reg31 <= 0;
	      slv_reg32 <= 0;
	      slv_reg33 <= 0;
	      slv_reg34 <= 0;
	      slv_reg35 <= 0;
	      slv_reg36 <= 0;
	      slv_reg37 <= 0;
	      slv_reg38 <= 0;
	      slv_reg39 <= 0;
	      slv_reg40 <= 0;
	      slv_reg41 <= 0;
	      slv_reg42 <= 0;
	      slv_reg43 <= 0;
	      slv_reg44 <= 0;
	      slv_reg45 <= 0;
	      slv_reg46 <= 0;
	      slv_reg47 <= 0;
	      slv_reg48 <= 0;
	      slv_reg49 <= 0;
	      slv_reg50 <= 0;
	      slv_reg51 <= 0;
	      slv_reg52 <= 0;
	      slv_reg53 <= 0;
	      slv_reg54 <= 0;
	      slv_reg55 <= 0;
	    end 
	  else begin
	    if (slv_reg_wren)
	      begin
	        case ( axi_awaddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	          6'h00:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 0
	                slv_reg0[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h01:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 1
	                slv_reg1[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h02:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 2
	                slv_reg2[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h03:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 3
	                slv_reg3[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h04:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 4
	                slv_reg4[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h05:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 5
	                slv_reg5[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h06:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 6
	                slv_reg6[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h07:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 7
	                slv_reg7[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h08:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 8
	                slv_reg8[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h09:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 9
	                slv_reg9[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h0A:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 10
	                slv_reg10[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h0B:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 11
	                slv_reg11[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h0C:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 12
	                slv_reg12[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h0D:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 13
	                slv_reg13[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h0E:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 14
	                slv_reg14[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h0F:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 15
	                slv_reg15[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h10:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 16
	                slv_reg16[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h11:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 17
	                slv_reg17[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h12:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 18
	                slv_reg18[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h13:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 19
	                slv_reg19[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h14:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 20
	                slv_reg20[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h15:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 21
	                slv_reg21[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h16:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 22
	                slv_reg22[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h17:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 23
	                slv_reg23[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h18:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 24
	                slv_reg24[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h19:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 25
	                slv_reg25[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h1A:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 26
	                slv_reg26[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h1B:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 27
	                slv_reg27[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h1C:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 28
	                slv_reg28[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h1D:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 29
	                slv_reg29[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h1E:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 30
	                slv_reg30[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h1F:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 31
	                slv_reg31[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h20:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 32
	                slv_reg32[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h21:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 33
	                slv_reg33[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h22:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 34
	                slv_reg34[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h23:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 35
	                slv_reg35[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h24:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 36
	                slv_reg36[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h25:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 37
	                slv_reg37[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h26:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 38
	                slv_reg38[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h27:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 39
	                slv_reg39[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h28:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 40
	                slv_reg40[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h29:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 41
	                slv_reg41[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h2A:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 42
	                slv_reg42[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h2B:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 43
	                slv_reg43[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h2C:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 44
	                slv_reg44[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h2D:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 45
	                slv_reg45[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h2E:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 46
	                slv_reg46[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h2F:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 47
	                slv_reg47[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h30:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 48
	                slv_reg48[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h31:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 49
	                slv_reg49[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h32:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 50
	                slv_reg50[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h33:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 51
	                slv_reg51[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h34:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 52
	                slv_reg52[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h35:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 53
	                slv_reg53[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h36:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 54
	                slv_reg54[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          6'h37:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 55
	                slv_reg55[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          default : begin
	                      slv_reg0 <= slv_reg0;
	                      slv_reg1 <= slv_reg1;
	                      slv_reg2 <= slv_reg2;
	                      slv_reg3 <= slv_reg3;
	                      slv_reg4 <= slv_reg4;
	                      slv_reg5 <= slv_reg5;
	                      slv_reg6 <= slv_reg6;
	                      slv_reg7 <= slv_reg7;
	                      slv_reg8 <= slv_reg8;
	                      slv_reg9 <= slv_reg9;
	                      slv_reg10 <= slv_reg10;
	                      slv_reg11 <= slv_reg11;
	                      slv_reg12 <= slv_reg12;
	                      slv_reg13 <= slv_reg13;
	                      slv_reg14 <= slv_reg14;
	                      slv_reg15 <= slv_reg15;
	                      slv_reg16 <= slv_reg16;
	                      slv_reg17 <= slv_reg17;
	                      slv_reg18 <= slv_reg18;
	                      slv_reg19 <= slv_reg19;
	                      slv_reg20 <= slv_reg20;
	                      slv_reg21 <= slv_reg21;
	                      slv_reg22 <= slv_reg22;
	                      slv_reg23 <= slv_reg23;
	                      slv_reg24 <= slv_reg24;
	                      slv_reg25 <= slv_reg25;
	                      slv_reg26 <= slv_reg26;
	                      slv_reg27 <= slv_reg27;
	                      slv_reg28 <= slv_reg28;
	                      slv_reg29 <= slv_reg29;
	                      slv_reg30 <= slv_reg30;
	                      slv_reg31 <= slv_reg31;
	                      slv_reg32 <= slv_reg32;
	                      slv_reg33 <= slv_reg33;
	                      slv_reg34 <= slv_reg34;
	                      slv_reg35 <= slv_reg35;
	                      slv_reg36 <= slv_reg36;
	                      slv_reg37 <= slv_reg37;
	                      slv_reg38 <= slv_reg38;
	                      slv_reg39 <= slv_reg39;
	                      slv_reg40 <= slv_reg40;
	                      slv_reg41 <= slv_reg41;
	                      slv_reg42 <= slv_reg42;
	                      slv_reg43 <= slv_reg43;
	                      slv_reg44 <= slv_reg44;
	                      slv_reg45 <= slv_reg45;
	                      slv_reg46 <= slv_reg46;
	                      slv_reg47 <= slv_reg47;
	                      slv_reg48 <= slv_reg48;
	                      slv_reg49 <= slv_reg49;
	                      slv_reg50 <= slv_reg50;
	                      slv_reg51 <= slv_reg51;
	                      slv_reg52 <= slv_reg52;
	                      slv_reg53 <= slv_reg53;
	                      slv_reg54 <= slv_reg54;
	                      slv_reg55 <= slv_reg55;
	                    end
	        endcase
	      end
	  end
	end    

	// Implement write response logic generation
	// The write response and response valid signals are asserted by the slave 
	// when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	// This marks the acceptance of address and indicates the status of 
	// write transaction.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_bvalid  <= 0;
	      axi_bresp   <= 2'b0;
	    end 
	  else
	    begin    
	      if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
	        begin
	          // indicates a valid write response is available
	          axi_bvalid <= 1'b1;
	          axi_bresp  <= 2'b0; // 'OKAY' response 
	        end                   // work error responses in future
	      else
	        begin
	          if (S_AXI_BREADY && axi_bvalid) 
	            //check if bready is asserted while bvalid is high) 
	            //(there is a possibility that bready is always asserted high)   
	            begin
	              axi_bvalid <= 1'b0; 
	            end  
	        end
	    end
	end   

	// Implement axi_arready generation
	// axi_arready is asserted for one S_AXI_ACLK clock cycle when
	// S_AXI_ARVALID is asserted. axi_awready is 
	// de-asserted when reset (active low) is asserted. 
	// The read address is also latched when S_AXI_ARVALID is 
	// asserted. axi_araddr is reset to zero on reset assertion.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_arready <= 1'b0;
	      axi_araddr  <= 32'b0;
	    end 
	  else
	    begin    
	      if (~axi_arready && S_AXI_ARVALID)
	        begin
	          // indicates that the slave has acceped the valid read address
	          axi_arready <= 1'b1;
	          // Read address latching
	          axi_araddr  <= S_AXI_ARADDR;
	        end
	      else
	        begin
	          axi_arready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_arvalid generation
	// axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	// S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	// data are available on the axi_rdata bus at this instance. The 
	// assertion of axi_rvalid marks the validity of read data on the 
	// bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	// is deasserted on reset (active low). axi_rresp and axi_rdata are 
	// cleared to zero on reset (active low).  
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rvalid <= 0;
	      axi_rresp  <= 0;
	    end 
	  else
	    begin    
	      if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
	        begin
	          // Valid read data is available at the read data bus
	          axi_rvalid <= 1'b1;
	          axi_rresp  <= 2'b0; // 'OKAY' response
	        end   
	      else if (axi_rvalid && S_AXI_RREADY)
	        begin
	          // Read data is accepted by the master
	          axi_rvalid <= 1'b0;
	        end                
	    end
	end    

	// Implement memory mapped register select and read logic generation
	// Slave register read enable is asserted when valid address is available
	// and the slave is ready to accept the read address.
	assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;
	always @(*)
	begin
	      // Address decoding for reading registers
	      case ( axi_araddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	        6'h00   : reg_data_out <= slv_reg0;
	        6'h01   : reg_data_out <= slv_reg1;
	        6'h02   : reg_data_out <= slv_reg2;
	        6'h03   : reg_data_out <= slv_reg3;
	        6'h04   : reg_data_out <= slv_reg4;
	        6'h05   : reg_data_out <= slv_reg5;
	        6'h06   : reg_data_out <= slv_reg6;
	        6'h07   : reg_data_out <= slv_reg7;
	        6'h08   : reg_data_out <= slv_reg8;
	        6'h09   : reg_data_out <= slv_reg9;
	        6'h0A   : reg_data_out <= slv_reg10;
	        6'h0B   : reg_data_out <= slv_reg11;
	        6'h0C   : reg_data_out <= slv_reg12;
	        6'h0D   : reg_data_out <= slv_reg13;
	        6'h0E   : reg_data_out <= slv_reg14;
	        6'h0F   : reg_data_out <= slv_reg15;
	        6'h10   : reg_data_out <= slv_reg16;
	        6'h11   : reg_data_out <= slv_reg17;
	        6'h12   : reg_data_out <= slv_reg18;
	        6'h13   : reg_data_out <= slv_reg19;
	        6'h14   : reg_data_out <= slv_reg20;
	        6'h15   : reg_data_out <= slv_reg21;
	        6'h16   : reg_data_out <= slv_reg22;
	        6'h17   : reg_data_out <= slv_reg23;
	        6'h18   : reg_data_out <= slv_reg24;
	        6'h19   : reg_data_out <= slv_reg25;
	        6'h1A   : reg_data_out <= slv_reg26;
	        6'h1B   : reg_data_out <= slv_reg27;
	        6'h1C   : reg_data_out <= slv_reg28;
	        6'h1D   : reg_data_out <= slv_reg29;
	        6'h1E   : reg_data_out <= slv_reg30;
	        6'h1F   : reg_data_out <= slv_reg31;
	        6'h20   : reg_data_out <= slv_reg32;
	        6'h21   : reg_data_out <= slv_reg33;
	        6'h22   : reg_data_out <= slv_reg34;
	        6'h23   : reg_data_out <= slv_reg35;
	        6'h24   : reg_data_out <= slv_reg36;
	        6'h25   : reg_data_out <= slv_reg37;
	        6'h26   : reg_data_out <= slv_reg38;
	        6'h27   : reg_data_out <= slv_reg39;
	        6'h28   : reg_data_out <= slv_reg40;
	        6'h29   : reg_data_out <= slv_reg41;
	        6'h2A   : reg_data_out <= slv_reg42;
	        6'h2B   : reg_data_out <= slv_reg43;
	        6'h2C   : reg_data_out <= slv_reg44;
	        6'h2D   : reg_data_out <= slv_reg45;
	        6'h2E   : reg_data_out <= slv_reg46;
	        6'h2F   : reg_data_out <= slv_reg47;
	        6'h30   : reg_data_out <= slv_reg48;
	        6'h31   : reg_data_out <= slv_reg49;
	        6'h32   : reg_data_out <= slv_reg50;
	        6'h33   : reg_data_out <= slv_reg51;
	        6'h34   : reg_data_out <= slv_reg52;
	        6'h35   : reg_data_out <= slv_reg53;
	        6'h36   : reg_data_out <= slv_reg54;
	        6'h37   : reg_data_out <= slv_reg55;
	        default : reg_data_out <= 0;
	      endcase
	end

	// Output register or memory read data
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rdata  <= 0;
	    end 
	  else
	    begin    
	      // When there is a valid read address (S_AXI_ARVALID) with 
	      // acceptance of read address by the slave (axi_arready), 
	      // output the read dada 
	      if (slv_reg_rden)
	        begin
	          axi_rdata <= reg_data_out;     // register read data
	        end   
	    end
	end    

	// Add user logic here
	// *********************Regs user declartion***********************
	// ---reference from parimal init decleartion 
	// (N=24) 
	// Reg 2*N : input selector regs 
	// 	     24 ~0 : 
	// 	     bit [index*5-1:index*5]: index of input lower 5 pins to comb logic
	// 	     29 ~25 : 
	//	     sheild's pin 0 select idx of "cfglut's out"
	// 	     30: input pins of comb locic select source from out of cfglut,
	// 	     	      1, select out of cfglut instance.0,select input pin from shield.
	// 	     31 : 1, select input patters as comb logic input.0: from pins
	// Reg 2*N+1 : initial value / patterns
	// 	     1) reg49.start [insts]  = 1 : initial value. 
	// 	     2) reg49.start [insts]  = 0 | reg49.done [insts] = 1 : 
	// 	     12~8 : output selector idx.
	// 	     4~0 : input patterns 
	//
	// Reg 48 :  cfg lut output control. 
	// 	     bit [pin index] : 1, tri-state; 0, output; 
	// Reg 49 :  recfg registers
	// 	     bit 31 : 1,recfg done.
	// 	     bit 23-0: each cfglut's recfg start,high valid.
	// Reg 50 :  bit 24~0 
	// 	     bit [index*5-1:index*5]: index of input higher 5 pins to comb logic
	//
	// *********************Configuration Assignment*********************
	parameter NUM_CFGLUTS = 24; // CFGLUT instance number
	parameter PINS_NUM_CFGLUT = 5; // CFGLUT5 pins number
	parameter MAX_IN_NUM = 9; // maximum inputs of comb logic

	//reg 2*N
	wire [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0]	cfg_sel_in_idxs [NUM_CFGLUTS-1:0] ;
	wire [PINS_NUM_CFGLUT-1:0]	cfg_sel_out_idxs [NUM_CFGLUTS-1:0] ;
	wire [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0]	cfg_sel_in_idxs_h_g12  ;
	wire [4*PINS_NUM_CFGLUT-1:0]	cfg_sel_in_idxs_h0_g0  ;
	wire [4*PINS_NUM_CFGLUT-1:0]	cfg_sel_in_idxs_h1_g0  ;
	assign	cfg_sel_in_idxs [ 0] = slv_reg0  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 1] = slv_reg2  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 2] = slv_reg4  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 3] = slv_reg6  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 4] = slv_reg8  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 5] = slv_reg10 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 6] = slv_reg12 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 7] = slv_reg14 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 8] = slv_reg16 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [ 9] = slv_reg18 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [10] = slv_reg20 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [11] = slv_reg22 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [12] = slv_reg24 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [13] = slv_reg26 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [14] = slv_reg28 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [15] = slv_reg30 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [16] = slv_reg32 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [17] = slv_reg34 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [18] = slv_reg36 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [19] = slv_reg38 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [20] = slv_reg40 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [21] = slv_reg42 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [22] = slv_reg44 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs [23] = slv_reg46 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	

	assign	cfg_sel_out_idxs [ 0] = slv_reg0  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 1] = slv_reg2  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 2] = slv_reg4  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 3] = slv_reg6  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 4] = slv_reg8  [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 5] = slv_reg10 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 6] = slv_reg12 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 7] = slv_reg14 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 8] = slv_reg16 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [ 9] = slv_reg18 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [10] = slv_reg20 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [11] = slv_reg22 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [12] = slv_reg24 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [13] = slv_reg26 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [14] = slv_reg28 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [15] = slv_reg30 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [16] = slv_reg32 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [17] = slv_reg34 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [18] = slv_reg36 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [19] = slv_reg38 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [20] = slv_reg40 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [21] = slv_reg42 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [22] = slv_reg44 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_sel_out_idxs [23] = slv_reg46 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4:PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	

	//higher 5 in pins 
	assign	cfg_sel_in_idxs_h_g12  = slv_reg50 [PINS_NUM_CFGLUT*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs_h0_g0   = slv_reg51 [4*PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_sel_in_idxs_h1_g0   = slv_reg52 [4*PINS_NUM_CFGLUT-1:0] ;	

	//
	wire [PINS_NUM_CFGLUT-1:0]	cfg_in_patterns [NUM_CFGLUTS-1:0] ; 	
	wire [PINS_NUM_CFGLUT-1:0]	cfg_in_patterns_h ; 	
	wire [0:0]	cfg_in_patterns_h_en ; 	
	assign	cfg_in_patterns [ 0] = slv_reg1  [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 1] = slv_reg3  [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 2] = slv_reg5  [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 3] = slv_reg7  [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 4] = slv_reg9  [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 5] = slv_reg11 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 6] = slv_reg13 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 7] = slv_reg15 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 8] = slv_reg17 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [ 9] = slv_reg19 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [10] = slv_reg21 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [11] = slv_reg23 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [12] = slv_reg25 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [13] = slv_reg27 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [14] = slv_reg29 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [15] = slv_reg31 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [16] = slv_reg33 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [17] = slv_reg35 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [18] = slv_reg37 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [19] = slv_reg39 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [20] = slv_reg41 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [21] = slv_reg43 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [22] = slv_reg45 [PINS_NUM_CFGLUT-1:0] ;	
	assign	cfg_in_patterns [23] = slv_reg47 [PINS_NUM_CFGLUT-1:0] ;	

	assign	cfg_in_patterns_h = slv_reg50 [(PINS_NUM_CFGLUT*PINS_NUM_CFGLUT+4):PINS_NUM_CFGLUT*PINS_NUM_CFGLUT] ;	
	assign	cfg_in_patterns_h_en = slv_reg50 [31] ;	
	//reg 2*N+1					    
	wire [31:0]	cfg_init_vals [NUM_CFGLUTS-1:0] ; 				   
	assign	cfg_init_vals [ 0] = slv_reg1  [31:0] ;	
	assign	cfg_init_vals [ 1] = slv_reg3  [31:0] ;	
	assign	cfg_init_vals [ 2] = slv_reg5  [31:0] ;	
	assign	cfg_init_vals [ 3] = slv_reg7  [31:0] ;	
	assign	cfg_init_vals [ 4] = slv_reg9  [31:0] ;	
	assign	cfg_init_vals [ 5] = slv_reg11 [31:0] ;	
	assign	cfg_init_vals [ 6] = slv_reg13 [31:0] ;	
	assign	cfg_init_vals [ 7] = slv_reg15 [31:0] ;	
	assign	cfg_init_vals [ 8] = slv_reg17 [31:0] ;	
	assign	cfg_init_vals [ 9] = slv_reg19 [31:0] ;	
	assign	cfg_init_vals [10] = slv_reg21 [31:0] ;	
	assign	cfg_init_vals [11] = slv_reg23 [31:0] ;	
	assign	cfg_init_vals [12] = slv_reg25 [31:0] ;	
	assign	cfg_init_vals [13] = slv_reg27 [31:0] ;	
	assign	cfg_init_vals [14] = slv_reg29 [31:0] ;	
	assign	cfg_init_vals [15] = slv_reg31 [31:0] ;	
	assign	cfg_init_vals [16] = slv_reg33 [31:0] ;	
	assign	cfg_init_vals [17] = slv_reg35 [31:0] ;	
	assign	cfg_init_vals [18] = slv_reg37 [31:0] ;	
	assign	cfg_init_vals [19] = slv_reg39 [31:0] ;	
	assign	cfg_init_vals [20] = slv_reg41 [31:0] ;	
	assign	cfg_init_vals [21] = slv_reg43 [31:0] ;	
	assign	cfg_init_vals [22] = slv_reg45 [31:0] ;	
	assign	cfg_init_vals [23] = slv_reg47 [31:0] ;	

	//reg 48 	
	wire [NUM_CFGLUTS-1:0]	cfg_pins_tri_out_en = slv_reg48 [NUM_CFGLUTS-1:0] ;
	//reg 49 					    
	wire		cfg_recfg_done ;
	wire [NUM_CFGLUTS-1:0]	cfg_recfg_start = slv_reg49 [NUM_CFGLUTS-1:0] ;					    

	//-------------------- registers assignments
	wire [NUM_CFGLUTS-1:0]	outs_v ;//outs of cfgluts.
	reg [NUM_CFGLUTS-1:0]	outs ;//outs of cfgluts.
	//inputs of cfgluts selector
	wire [NUM_CFGLUTS-1:0]	cfg_sel_in_mod_outs  ; 
	wire [NUM_CFGLUTS-1:0]	cfg_sel_in_mod_pattern  ; 
					    //bit0 : sel from out of cfgluts;
					    //bit1 : sel from cfg patterns;
					    //default : sel from in pins;
//	assign  cfg_sel_in_mod_outs = slv_reg48 [NUM_CFGLUTS-1:0];  
	assign  cfg_sel_in_mod_outs = { //---0814 fix out->in configuration
	 				slv_reg46 [30] ,	
	 				slv_reg44 [30] ,	
	 				slv_reg42 [30] ,	
					slv_reg40 [30] ,	
					slv_reg38 [30] ,	
					slv_reg36 [30] ,	
					slv_reg34 [30] ,	
					slv_reg32 [30] ,	
					slv_reg30 [30] ,	
					slv_reg28 [30] ,	
					slv_reg26 [30] ,	
					slv_reg24 [30] ,	
					slv_reg22 [30] ,	
					slv_reg20 [30] ,	
					slv_reg18 [30] ,	
					slv_reg16 [30] ,	
					slv_reg14 [30] ,	
					slv_reg12 [30] ,	
					slv_reg10 [30] ,	
					slv_reg8  [30] ,	
					slv_reg6  [30] ,	
					slv_reg4  [30] ,	
					slv_reg2  [30] ,	
					slv_reg0  [30] };	

	assign	cfg_sel_in_mod_pattern = { 
	 				slv_reg46 [31] ,	
	 				slv_reg44 [31] ,	
	 				slv_reg42 [31] ,	
					slv_reg40 [31] ,	
					slv_reg38 [31] ,	
					slv_reg36 [31] ,	
					slv_reg34 [31] ,	
					slv_reg32 [31] ,	
					slv_reg30 [31] ,	
					slv_reg28 [31] ,	
					slv_reg26 [31] ,	
					slv_reg24 [31] ,	
					slv_reg22 [31] ,	
					slv_reg20 [31] ,	
					slv_reg18 [31] ,	
					slv_reg16 [31] ,	
					slv_reg14 [31] ,	
					slv_reg12 [31] ,	
					slv_reg10 [31] ,	
					slv_reg8  [31] ,	
					slv_reg6  [31] ,	
					slv_reg4  [31] ,	
					slv_reg2  [31] ,	
					slv_reg0  [31] };	

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
		outs <= {NUM_CFGLUTS{1'b0}} ; 
	  else
		outs <= outs_v ;
	end
	// *********************Instance : Input Mux***********************
	reg [PINS_NUM_CFGLUT-1:0]	ins [NUM_CFGLUTS-1:0] ; //input inpis of cfgluts
	reg [3:0]	ins_h0_g0 ; //higher ins for 0st level's mux
	reg [3:0]	ins_h1_g0 ; //higher ins for 0st level's mux
	reg [1:0]	ins_h_g1  ; //higher ins for 1st level's mux
	reg 		ins_h_g2 ; //higer ins for 2st level's mux

	always @* begin	: B_INS_SEL
		integer i ;//,j;
		for (i=0;i<NUM_CFGLUTS;i=i+1) begin
//			for (j=0;j<5;j=j+1) begin
				ins [i][0]= (cfg_sel_in_mod_outs[i]    		) ? outs[ cfg_sel_in_idxs[i][ (5*1 -1) : 0 ]] :  // cfg : out -> in 
					    (cfg_sel_in_mod_pattern[i]		) ? cfg_in_patterns [i][0] :  // cfg : pattern -> in 
					    					    PINS_SHIELD_I [ cfg_sel_in_idxs[i][ (5*1 -1) : 0 ] ] ; // cfg : in pin -> in
				ins [i][1]= (cfg_sel_in_mod_pattern[i]		) ? cfg_in_patterns [i][1] : 
					    					    PINS_SHIELD_I [ cfg_sel_in_idxs[i][ (5*2 -1) : 5*1 ] ] ;
				ins [i][2]= (cfg_sel_in_mod_pattern[i]		) ? cfg_in_patterns [i][2] : 
					   					   PINS_SHIELD_I [ cfg_sel_in_idxs[i][ (5*3 -1) : 5*2 ] ] ;
				ins [i][3]= (cfg_sel_in_mod_pattern[i]		) ? cfg_in_patterns [i][3] : 
					    					   PINS_SHIELD_I [ cfg_sel_in_idxs[i][ (5*4 -1) : 5*3 ] ] ;
				ins [i][4]= (cfg_sel_in_mod_pattern[i]		) ? cfg_in_patterns [i][4] : 
					    					   PINS_SHIELD_I [ cfg_sel_in_idxs[i][ (5*5 -1) : 5*4 ] ] ;
//			end
		end
	end

	always @* begin	: B_INS_SEL_H_MUX
		ins_h0_g0 [0]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [0] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h0_g0[ (5*1 -1) : 0 ] ] ; // cfg : in pin -> in
		ins_h0_g0 [1]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [0] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h0_g0[ (5*2 -1) : 5*1 ] ] ; // cfg : in pin -> in
		ins_h0_g0 [2]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [0] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h0_g0[ (5*3 -1) : 5*2 ] ] ; // cfg : in pin -> in
		ins_h0_g0 [3]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [0] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h0_g0[ (5*4 -1) : 5*3 ] ] ; // cfg : in pin -> in

		ins_h1_g0 [0]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [1] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h1_g0[ (5*1 -1) : 0 ] ] ; // cfg : in pin -> in
		ins_h1_g0 [1]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [1] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h1_g0[ (5*2 -1) : 5*1 ] ] ; // cfg : in pin -> in
		ins_h1_g0 [2]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [1] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h1_g0[ (5*3 -1) : 5*2 ] ] ; // cfg : in pin -> in
		ins_h1_g0 [3]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [1] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h1_g0[ (5*4 -1) : 5*3 ] ] ; // cfg : in pin -> in

		ins_h_g1 [0]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [2] :  // cfg : pattern -> in 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h_g12[ (5*1 -1) : 0 ] ] ; // cfg : in pin -> in
		ins_h_g1 [1]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [2] : 
			    					    PINS_SHIELD_I [ cfg_sel_in_idxs_h_g12[ (5*2 -1) : 5*1 ] ] ;
		ins_h_g2    = (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [3] : 
			   					   PINS_SHIELD_I [ cfg_sel_in_idxs_h_g12[ (5*3 -1) : 5*2 ] ] ;
//		ins_h [3]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [3] : 
//			    					   PINS_SHIELD_I [ cfg_sel_in_idxs_h12[ (5*4 -1) : 5*3 ] ] ;
//		ins_h [4]= (cfg_in_patterns_h_en		) ? cfg_in_patterns_h [4] : 
//			    					   PINS_SHIELD_I [ cfg_sel_in_idxs_h12[ (5*5 -1) : 5*4 ] ] ;
	end

	// *********************Instance : CFGLUTs ***********************
	reg [NUM_CFGLUTS-1:0]	cfg_recfg_start_ff ;
	always @( posedge S_AXI_ACLK ) begin
	  if ( S_AXI_ARESETN == 1'b0 )
		cfg_recfg_start_ff <= {NUM_CFGLUTS{1'b0}} ; 
	  else
		cfg_recfg_start_ff <= cfg_recfg_start ;
	end

	wire [NUM_CFGLUTS-1:0]	cfg_recfg_start_pulse = cfg_recfg_start & ~cfg_recfg_start_ff    ;

	//generate instances for re-cfg fms and cfglut primitive
	wire	[NUM_CFGLUTS-1:0]	cdis ;
	wire	[NUM_CFGLUTS-1:0]	ces ;
	wire	[NUM_CFGLUTS-1:0]	dones ;

	genvar i ;
	generate 
		for (i=0; i<NUM_CFGLUTS;i=i+1) 
		begin
			cfglut_fsm u_cfglut_fsm ( 
						.clk 		(S_AXI_ACLK ),
						.start 		(cfg_recfg_start_pulse[i] ),
						.fn_init_value 	(cfg_init_vals[i] ),
						.cdi 		(cdis[i]),
						.ce		(ces[i]),
						.done 		(dones[i]) 
						) ;
    
	    		CFGLUT5 #(
       				.INIT(32'h00000001) // Specify initial LUT contents - user : default 0 idx 1
			    ) U_CFGLUT5 (
			       .CDO(), // Reconfiguration cascade output
			       .O5(),//result),   // 4-LUT output
			       .O6(outs_v[i]),   // 5-LUT output
			       .CDI(cdis[i]), // Reconfiguration data input
			       .CE(ces[i]),   // Reconfiguration enable input
			       .CLK(S_AXI_ACLK ), // Clock input
			       .I0(ins[i][0]),   // Logic data input
			       .I1(ins[i][1]),   // Logic data input
			       .I2(ins[i][2]),   // Logic data input
			       .I3(ins[i][3]),   // Logic data input
			       .I4(ins[i][4])    // Logic data input
			    );
		end

	endgenerate

	// *********************Instance : Output Mux***********************
	assign PINS_SHIELD_OUT_EN = cfg_pins_tri_out_en [23:0] ; 

	// User logic ends
	// MUX16 output from cfglut0~15 
	wire [3:0] outs_mux4; 	
	wire [1:0] outs_mux8; //mux2 for 8 inputs
	wire outs_mux16; 	

	wire [3:0] outs_gp0 = outs [3:0] ;
	wire [3:0] outs_gp1 = outs [7:4] ;
	wire [3:0] outs_gp2 = outs [11:8] ;
	wire [3:0] outs_gp3 = outs [15:12] ;
	assign outs_mux4[0] = outs_gp0 [ {ins_h1_g0[0],ins_h0_g0[0]} ] ;
	assign outs_mux4[1] = outs_gp1 [ {ins_h1_g0[1],ins_h0_g0[1]} ] ;
	assign outs_mux4[2] = outs_gp2 [ {ins_h1_g0[2],ins_h0_g0[2]} ] ;
	assign outs_mux4[3] = outs_gp3 [ {ins_h1_g0[3],ins_h0_g0[3]} ] ;

	//2th level mux (mux2 for 8 inputs)
	assign outs_mux8[0] = (ins_h_g1[0]) ? outs_mux4[1] : outs_mux4 [0] ;
	assign outs_mux8[1] = (ins_h_g1[1]) ? outs_mux4[3] : outs_mux4 [2] ;

	//3th level mux (mux2 for 9 inputs)
	//assign outs_mux16 = outs_mux4 [ ins_h[3:2]  ] ;
	assign outs_mux16 = (ins_h_g2) ? outs_mux8[1] : outs_mux8[0] ;

	wire [31:0]  outs_premap = {{(32-NUM_CFGLUTS-4-3){1'b0}},outs_mux16,outs_mux8[1:0],outs_mux4[3:0],outs[NUM_CFGLUTS-1:0]}  ; 
	// -------------------pins out selector------------
	reg [NUM_CFGLUTS-1:0]	pin_outs ;
	always @* begin : B_PIN_OUTS
		integer i ;
		for (i=0;i<NUM_CFGLUTS;i=i+1)
		pin_outs [i] = (cfg_pins_tri_out_en[i]) ? outs_premap [ cfg_sel_out_idxs [i] ] : 1'b0 ; 
	end
//	assign PINS_SHIELD_O = pin_outs [23:0] ;
	always @( posedge S_AXI_ACLK ) begin
	  if ( S_AXI_ARESETN == 1'b0 )
		PINS_SHIELD_O <= 24'h0 ;
	  else
		PINS_SHIELD_O <= pin_outs [23:0] ;
	end
	// User logic ends

	endmodule


//---Reuse from xilinx 
//////////////////////////////////////////////////////////////////////////////////
// Module Name: cfglut_fsm
//////////////////////////////////////////////////////////////////////////////////

module cfglut_fsm(
    input clk,
    input start,
    input [(2**COUNTER_WIDTH)-1:0] fn_init_value,
    output reg cdi,
    output reg ce,
    output reg done
    );
    
    parameter COUNTER_WIDTH = 5;
    
    reg [(2**COUNTER_WIDTH)-1:0] fn_init_value_i;
    reg [COUNTER_WIDTH-1:0] cnt = {COUNTER_WIDTH{1'b0}};
    
    wire done_i;
    
    assign done_i = (cnt == (2**COUNTER_WIDTH-1)) ? 1'b1 : 1'b0;
    
    always @(posedge clk)
        done <= done_i;
    
    always @(posedge clk)
    begin
        if(start)
        begin
            ce <= 1'b1;
            cdi <= 1'b0;
            fn_init_value_i <= fn_init_value;
        end
        else if (done)
        begin
            ce <= 1'b0;
            cdi <= 1'b0;
            fn_init_value_i <= 32'b0;
        end
        else
        begin
            ce <= ce;
            cdi <= fn_init_value_i[31];
            fn_init_value_i = {fn_init_value_i[30:0], 1'b0};
        end
    end

   always @(posedge clk)
      if (!ce)
         cnt <= {COUNTER_WIDTH{1'b0}};
      else if (ce)
         cnt <= cnt + 1'b1;
    
endmodule
